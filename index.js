function guardarDatosUsuario(){
  var nombre= document.getElementById("txtNombre").value;
  var email= document.getElementById("email").value;
  var dni= document.getElementById("dni").value;
  //alert(nombre);
  localStorage.setItem("nombre",nombre);
  localStorage.setItem("email",email);
  localStorage.setItem("dni",dni);
  sessionStorage.setItem("nombre",nombre);
  sessionStorage.setItem("email",email);
  sessionStorage.setItem("dni",dni);
  
  var usuario ={"nombre":nombre,"email":email,"dni":dni};
  localStorage.setItem("usuarioST",JSON.stringify(usuario));
  sessionStorage.setItem("usuarioST",JSON.stringify(usuario));
}

function recuperarDatosUsuario(){
  var nombreLocal = localStorage.getItem("nombre");
  var emailLocal = localStorage.getItem("email");
  var dniLocal = localStorage.getItem("dni");
  var nombreSession = sessionStorage.getItem("nombre");
  var emailSession = sessionStorage.getItem("email");
  var dniSession = sessionStorage.getItem("dni");
  var usuarioLocal =localStorage.getItem("usuarioST");
  
  alert("Nombre Local:" + nombreLocal + " Nombre Session:" + nombreSession);
  alert("Email Local:" + emailLocal + " Email Session:" + emailSession);
  alert("DNI Local:" + dniLocal + " DNI Session:" + dniSession);
}